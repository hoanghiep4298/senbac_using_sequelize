class Page{
    constructor(pageNumber, limitNumber) {
        this.page = 1;       //current page
        if(pageNumber) this.page = parseInt(pageNumber); 

        this.limit = 3;     // num of row in a page
        if(limitNumber) this.limit = parseInt(limitNumber);

        this.offset = this.limit * (this.page - 1); // offset for current page

        //let body = { page, limit, offset};
    }

    static countPage(totalRow){
        return Math.ceil(totalRow / this.limit); // total page
    }
}

module.exports = { Page };