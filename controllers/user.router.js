const express = require('express');
const { UserService } = require("../services/user.service")
const userRouter = express.Router();

userRouter.post('/create', (req, res) => {
	
	const user = {
		name: req.body.name
	}

	UserService.create( user )
	.then( result => res.send( result ))
	.catch( err => console.log(err) )

	
})

userRouter.get('/getall', (req, res)=> {
    
	let pageNumber = req.query.pagenumber;
    let limitNumber = req.query.limitnumber;
    let searchCondition = req.query.searchCondition;

	UserService.getAll(pageNumber, limitNumber, searchCondition)
	.then(data => res.send(JSON.stringify(data)))
	.catch(err => console('getall err', err)) 
})


userRouter.put('/update', (req, res) => {
	let id = req.body.id;
	let name = req.body.name;

	UserService.update(id, name)
		.then(data => res.send(JSON.stringify(data)))
		.catch(err => console.log(err))

})

userRouter.delete('/delete', (req, res) => {
	let id = req.body.id;

	UserService.delete(id)
		.then(data => res.send(JSON.stringify(data)))
		.catch(err => console.log(err))

})

module.exports = userRouter;