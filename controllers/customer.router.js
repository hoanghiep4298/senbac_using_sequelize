const express = require('express')
const { CustomerService } = require('../services/customer.service')
const customerRouter = express.Router()

customerRouter.post('/create', (req, res)=> {

    const customer = {
    	name: req.body.name,
    	userId: req.body.userId
    }

    CustomerService.create(customer)
    .then(result => res.send(result))
    .catch(err => console.log(err))
})

customerRouter.get('/getall', (req, res) => {
    let pageNumber = req.query.pagenumber;
    let limitNumber = req.query.limitnumber;
    let searchCondition = req.query.searchCondition;

   // console.log(pageNumber, limitNumber, searchCondition)
	CustomerService.getAll(pageNumber, limitNumber, searchCondition)
	.then(data => res.send(JSON.stringify(data)))
	.catch(err => console('getall err', err))
})

customerRouter.put('/update', (req, res) => {
    let id = req.body.id;
    let name = req.body.name;
    //console.log(id, name)

    CustomerService.update(id, name)
    .then(data => res.send(JSON.stringify(data)))
    .catch(err => console.log(err))

})

customerRouter.delete('/delete', (req, res) => {
    let id = req.body.id;

    CustomerService.delete(id)
        .then(data => res.send(JSON.stringify(data)))
        .catch(err => console.log(err))

})

module.exports = customerRouter;