const express = require('express');
var bodyParser = require('body-parser')
const db = require('./models')

var userRouter = require('./controllers/user.router')
var customerRouter = require('./controllers/customer.router')

var port = 3000;
var app = express()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));


//set header
app.use(function(req, res, next) {
  	res.setHeader('Content-Type', 'application/json');
	res.setHeader("Access-Control-Allow-Credentials","true");
    res.setHeader("Access-Control-Allow-Headers","Content-Type");
  next();
});
// USER
app.use('/users', userRouter);
//CUSTOMER
app.use('/customers', customerRouter);

//-----------------------


// chay lan dau phai sync truoc
db.sequelize.sync()
	.then(() => {
		console.log('connected...')	
	})
	.catch(e => console.log(e));


app.listen(port, function() {
	console.log(`Server listening on port ${port}`)
})


// 