'use strict';

module.exports = (sequelize, Datatype) => {
	var Customer = sequelize.define('customer', {
		
		name:{
			type: Datatype.STRING(30),
		}

	}
	// {
	// 	freezeTableName: true
	// }

	);

	Customer.associate = function(models) {
		models.customer.belongsTo(models.user);
	}

	return Customer;
}

