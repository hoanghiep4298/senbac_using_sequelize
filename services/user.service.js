const db = require('../models');
const { Page } = require('../helpers/paging')
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class UserService{
	static async create(user) {
		let result = '';
		console.log("thong tin user", user)
		try{
			if(!user){
				result = 'invalid user';
				return result;
			}
			//check user existing
			const userExisted = await db.user.findOne( { where: { name: user.name } } )
			if(userExisted) {
				result = 'name is existed';			
				return result;
			} else {
				console.log("ten chua ton tai")
			}

			// create user
			const userCreating = await db.user.create({
				// undefined for auto increasing PK index
				name: user.name
			})

			if(userCreating){
				result = 'create user successful'
				return result;
			}

		}
		catch(err) {
			console.log(err);
			result = 'error';
			return result;
		}

	}

	static async getAll(pageNumber, limitNumber, searchCondition){
		let data = {}
		if(!searchCondition){
			searchCondition = "";
		}
		try {
			const paging = new Page(pageNumber, limitNumber);
			const users = await db.user.findAndCountAll({
				where: { name:  { [Op.like]: `%${ searchCondition }%` } },
				limit: paging.limit,
			 	offset: paging.offset
			});
			
			//paging 
			// const paging = await Page.paging(pageNumber, limitNumber, userCount.count);
			// let users = userCount.rows.slice((paging.page-1)*paging.limit, (paging.page-1)*paging.limit+paging.limit)

			if(users) {
				data = {
					body: users,
					total_customer: users.count,
					total_page: Page.countPage(users.count),
					current_page: paging.page,
				}
				return data
			}
		}
		catch(err){
			console.log(err)
		}

	}

	// update
	static async update(idUser, nameUser){
		let id = parseInt(idUser)
		let name 
		if(nameUser) name = nameUser.trim()

		if(!id  || !Number.isInteger(id) || id <= 0){
			console.log('ID khong hop le')
		}
		
		if( name === '' || !(/^[a-zA-Z ]{2,30}$/.test(name)) ) {
			return "invalid name"
		}

		try {
			const existedUser = await db.user.findOne({ where: { id } })
			if(existedUser){
				await db.user.update({
					name: name
				},{
					where: { id }
				})

				return "update successful"
			} else {
				return "user khong ton tai";
			}
		}
		catch(err){
			console.log(err)
		}
	}

	//delete
	static async delete(idCustomer){

		let id =  parseInt(idCustomer)
		if(!id  || !Number.isInteger(id) || id <= 0){
			return('ID khong hop le')
		}

		try {
			//check reference to customer before delete
			let customerInfo = await db.customer.findOne({ where: { userId: id } })

			if(customerInfo) {
				//console.log(customerInfo)
				return('user ton tai lien he voi customer')
			}

			await db.user.destroy({
				where: {id}
			})
			return "deleted"
		}
		catch(err){
			return "delete fail"
			console.log(err)
		}
	}


	//check reference to customer before delete
}

module.exports = { UserService };