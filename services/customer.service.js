const db = require("../models")
const { Page } = require("../helpers/paging")
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class CustomerService{
	// create customer
	static async create(customer){
		let result = '';
		//check invalid
		console.log("thong tin customer", customer)

		try {
			if(!customer){
				result = 'customer invalid';
				return result;
			}
			//check existed
			// const customerExisted = await db.customer.findOne({ where: {name: customer.name} });
			// console.log("log find customer", customerExisted)
			// if(customerExisted){
			// 	result = 'customer is existed'
			// 	return result
			// } else {
			// 	console.log("ten chua ton tai");
			// }

			//create customer
			const customerCreating = await db.customer.create({
				//id: undefined, //undefined for auto increasing index
				name: customer.name,
				userId: customer.userId
			})


			if(customerCreating){
				result = 'creating successful'
				return result;
			}
		}
		catch(err){
			result = 'error'
			console.log(err);
			return result;
		}
	}

	// get all customer
	static async getAll(pageNumber, limitNumber, searchCondition){
		let data = {}
		if (!searchCondition){
			searchCondition = "";
		}
		try {
			const paging = new Page(pageNumber, limitNumber);

			// db.user.hasMany(db.customer,{foreignKey: 'id'});
			// db.customer.belongsTo(db.user, {foreignKey: 'userid'})

			const customers = await db.customer.findAndCountAll({
				include: [{
					model: db.user,
					required: false
					
				}],
				where: { name:  { [Op.like]: `%${ searchCondition }%` } },
				limit: paging.limit,
			 	offset: paging.offset
			});

			//console.log("page", paging.page, "limit ", paging.limit)
			// sub arr customer for paging
			//let customers = customerCount.rows.slice((paging.page-1)*paging.limit, (paging.page-1)*paging.limit+paging.limit)

			if(customers) {
				data = {
					body: customers,
					total_customer: customers.count,
					total_page: Page.countPage(customers.count),
					current_page: paging.page,
				}
				return data
			}
		}
		catch(err){
			console.log(err)
		}

	}

	//update
	static async update(idCustomer, nameCustomer){
		let id = parseInt(idCustomer)
		let name
		if(nameCustomer) name = nameCustomer.trim()

		if(!id  || !Number.isInteger(id) || id <= 0){
			return('invalid ID ')
		}

		if( name === '' || !(/^[a-zA-Z ]{2,30}$/.test(name)) ) {
			return "invalid name"
		}

		try {
			const existedCustomer = await db.customer.findOne({ where: { id } })
			if(existedCustomer){
				await db.customer.update({
					name: name
				},{
				where: { id }
				})

				return "update successful"
			} else {
				return "customer is not existed";
			}
		} 
		catch(err){
			console.log(err)
		}
	}

	//remove
	static async delete(idCustomer){

		let id = parseInt(idCustomer)
		if(!id  || !Number.isInteger(id) || id <= 0){
			return('ID khong hop le')
		}

		try {
			await db.customer.destroy({
				where: { id }
			})
			return "deleted"
		}
		catch(err){
			return "delete fail"
			console.log(err)
		}
	}
}

module.exports = { CustomerService };